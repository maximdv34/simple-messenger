import React from 'react';
import { Avatar } from '../components/Avatar';
import { Button } from '../components/Button';
import { Input } from '../components/Input';
import '../styles/Registration.scss';
export default function Registration(): JSX.Element {
    return(
    <>
    <form className='ContainerRegistration'>
        <div className='title'>Registration</div>
        <Input type="text">Login</Input>
        <Input type="email">Email</Input>
        <Input type="password">Password</Input>
        <Input type="password">Repeat password</Input>
        <div className='blockRectangle'>
            <div>Logo</div>
            <div className='rectangle'>
                <Avatar size='big' alt='FL'/>
                <Input type="text">url</Input>
            </div>
        </div>
        <div className='blockButton'>
            <Button type='submit'>Register</Button>
        </div>
    </form>
    </>   
    );
}