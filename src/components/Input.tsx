import React from 'react';
import styles from '../styles/Input.module.scss';
import clsx from "clsx";
export function Input(props: { 
    children: string | React.ReactElement<any, string | React.JSXElementConstructor<any>>; 
    type: string | (string & {}); 
    placeholder?: string; 
    formContext?: JSX.IntrinsicAttributes & React.ClassAttributes<HTMLInputElement> & React.InputHTMLAttributes<HTMLInputElement>;
    error?: string; 
    }): JSX.Element {
    return(
    <>
    <div className={props.error ? clsx(styles.blockInput, styles.blockInputError) : styles.blockInput}>
        <div className={styles.text}>{props.children}</div>
        <input type={props.type}  placeholder={props.placeholder} {...props.formContext}/>
        <div className={styles.error}>{props.error}</div>
    </div>
    </>   
    );
}