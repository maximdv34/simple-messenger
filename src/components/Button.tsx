import React from 'react';
import styles from '../styles/Button.module.scss';
import clsx from "clsx";
export function Button(props: { 
    children: string | React.ReactElement<any, string | React.JSXElementConstructor<any>>; 
    type: 'submit' | 'reset' | 'button' | undefined;
    color? : "primary" | "secondary";
    }): JSX.Element {
    return(
    <>
    <button className={props.color==="secondary" ? clsx(styles.ButtonMain, styles.ButtonMainSecondary) : styles.ButtonMain} 
    type={props.type}>
        {props.children}
    </button>
    </>   
    );
}