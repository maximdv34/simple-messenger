import React from 'react';
import styles from '../styles/Avatar.module.scss';
import clsx from "clsx";
export function Avatar(props: { 
    size: 'small' | 'big';
    src?: string;
    alt: string;
    }): JSX.Element {
    return(
    <>
    <div className={props.size==="small" ? clsx(styles.blockAvatar, styles.blockAvatarSmall) : clsx(styles.blockAvatar, styles.blockAvatarBig)}>
        <img src={props.src} alt={props.alt}></img>
    </div>
    </>   
    );
}