import React from 'react';
import { gql, useQuery } from '@apollo/client';
import { Routes, Route } from "react-router-dom";
import './styles/App.scss';
import { Input } from './components/Input';
import { Button } from './components/Button';
import { Avatar } from './components/Avatar';
import Registration from './pages/Registration';
const GET_GREETING = gql`
{

  hello

}
`;
export default function App() {
  const { loading, error, data } = useQuery(GET_GREETING);
  return (
    <>
    {/* <div className='mainContainer'>
      <div className='contentContainer'>
        <div>Hello</div>
        <div>{data}</div>
        <Input type="text" placeholder='Something'>Text1</Input>
        <Input type="text" error="error">Text2</Input>
        <Button type='submit'>Something</Button>
        <Button type='submit' color='secondary'>Something</Button>
        <Avatar size='big' alt='FL' src='/images/image1.svg'/>
        <Avatar size='big' alt='FL' />
        <Avatar size='small' alt='FL' src='/images/image1.svg'/>
        <Avatar size='small' alt='FL' />
      </div>
    </div> */}
    <div className='mainContainer'>
      <div className='contentContainer'>
        <Routes>
          <Route path='/Registration' element={<Registration/>}/>
        </Routes>
      </div>
    </div>
    </>
  );
}
